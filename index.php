<!DOCTYPE html>
<html lang="pt-BR">
    <head>
       <meta charset="utf-8">
       <title> Login de Usuário </title>
    </head>
    <body>
        <form action="validacao.php" method="post">
          <fieldset>
          <legend>Dados de Login</legend>
              <label for="txUsuario">Usuário</label>
              <input type="text" name="usuario" id="txUsuario" maxlength="25" />
              <label for="txSenha">Senha</label>
              <input type="password" name="senha" id="txSenha" pattern="[A-Z]{1}[0-9]{7}"/>

              <input type="submit" value="Entrar" />
          </fieldset>
        </form>
    </body>
</html>