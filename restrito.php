<?php 
    // A sessão precisa ser iniciada em cada página diferente
  if (!isset($_SESSION)) session_start();

    //Recebe o nível de usuario e armazena na variável
  $nivel_usuario = $_SESSION['UsuarioNivel'];
    
  // Verifica se não há a variável da sessão que identifica o usuário
  if (!isset($_SESSION['UsuarioID'])) {
      // Destrói a sessão por segurança
      session_destroy();
      // Redireciona o visitante de volta pro login
      header("Location: index.php"); exit;
  }

  // Testa se o nível do usuário é 1 ou 2
  switch($nivel_usuario){
      case 1:
          echo "
          <html lang='pt-BR'>
           <head>
           <meta charset='utf-8'>
           </head>
            <h1>Página restrita</h1>
            <p>Olá, meu nome é ".$_SESSION['UsuarioNome']." </p>
            <p>Eu sou Usuário Comum do sistema</p>
          </html>\n";
          break;
      case 2:
          echo "
          <html lang='pt-BR'>
           <head>
           <meta charset='utf-8'>
           </head>
            <h1>Página restrita</h1>
            <p>Olá, meu nome é ".$_SESSION['UsuarioNome']." </p>
            <p>Eu sou Usuário Administrador do sistema</p>
          </html>\n";
          break;
  }
?>

<form action="logout.php" method="get">
    <input type="submit" value="Sair" />
</form>
